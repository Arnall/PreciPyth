# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

def inputs():
    """Returns a Python dictionary which contains all the parameters and numerical
     values used by the software (eg. composition, lattice parameter, temperature, ...).

     Feel free to modify this file to change the conditions of the simulation, 
     it's yours ! Depending on the parameters you choose, errors (or magic !) may occur. 

	 Most of the time, errors are caused by a non-physical set of parameters. For 
	 example, if you use a very low value for the matrix/precipitate interface energy, 
	 the model will consider there's virtually no energetic cost for creating a 
	 high number of small precipitates, resulting in extreme values of dN/dt and instability.
	 
	 You can go back to a working configuration file at any moment by downloading 
	 the sources available at the address https://gitlab.com/Arnall/PreciPyth.

	 Feel free to share and compare you configuration files with other users !
    """

    # Constants (used to improve readability)
    hour = 3600
    day = 24 * hour
    yr = 365 * day
    binary = ['nuc_simp', 'gr_simp']
    ternary = ['nuc_mini', 'gr_sys']

    model_configuration = {
    # Dictionary containing input values of the model.

    ############
    # Parameters to be used
    'Temperature' : 380 + 273., # in Kelvins
    'composition_name' : 'A2',
    'CSc_Matrix_initial' : 0.06 / 100,
    'CZr_Matrix_initial' : 0.05 / 100,
    'Surface_Energy_Sc' : 0.232,
    'Surface_Energy_Zr' : 0.3,
    't_min' : 1E-6,
    't_max' : 15 * day,
    'n_init' : 1E20,
    'r_init' : 1.4E-9,

    # 'methods' : [binary], # use this one to stick to either of the two methods
    # 'methods_names' : ['Binary'],
    'methods' : [binary, ternary], # use this one to compare the 2 methods
    'methods_names' : ['Binary','Ternary'],

    ########
    ### Choice of the routines to use (comment/uncomment the following entries)
    ### For isothermal :
    'Run_isothermal' : '',

    ### For TTT diagram in a range of temperatures [T_min, T_max, T_step] :
    # 'Temperature_range' : [300, 310, 5],

    ### For multi-steps heat treatment
    # structure of : steps = [[time1, Temperature1], [time2, Temperature2], ...]
    # 'steps' : [[10*hour, 250], [12*hour, 300], [8*hour, 350]],

    ### User Settings
    'showGraph' : True, 
    'saveGraph' : False, # save the PDF
    'saveData' : False, # save the csv file
    'path_SANS' : '../data/fit.csv', # path to the SANS results file

    ############################
    # Thermodynamical parameters - specific to the case of study.
    'Vat_Al3Sc' : 1.732E-29,
    'Vat_Al3Zr' : 1.703E-29,
    'LATTICE_PARAMETER' :  4.05E-10, # LP of the aluminum matrix 
    'ALPHA_SC' : 0.959, # V_atMatrix/V_atAl3Sc = (a_mat^3)/4 / (a_Al3sc^3)/4 
    'ALPHA_ZR' : 0.975, # V_Al/V_Zr

    # The diffusion coefficient is of the form : PRE_FACTOR_DIFF_SC*exp(-BARRIER_DIFF_SC/8.314/Temperature)
    'PRE_FACTOR_DIFF_SC' : 0.000265,
    'PRE_FACTOR_DIFF_ZR' : 0.0728,
    'BARRIER_DIFF_SC' : 168000,
    'BARRIER_DIFF_ZR' : 242000,

    # Solubility product is of the form log10(K(T)) = -A/T+B
    'solubility_A_Sc' : 2696.6382,
    'solubility_B_Sc' : 0.0228,
    'solubility_A_Zr' : 2711.8238,
    'solubility_B_Zr' : -0.3811
    }

    return model_configuration