[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Arnall%2FPreciPyth/binder?filepath=notebooks%2Fal-sc-zr-precipitation.ipynb)

# PreciPyth

**PreciPyth** is a research code written in Python by Arnaud Allera (2018).
It aims to model the precipitation of Scandium and Zirconium in aluminum alloys, and can be useful in both experiment design and understanding of experimental results.

❎ **Disclaimer : this code is still under development and provided as it is. There is NO warranty of any kind, any use of the provided results is at your own risk.**

#### Approach
It computes [classical nucleation and growth theories](http://michel.perez.net.free.fr/Perez08.pdf) calculations for precipitation with a "mean radius" approach. With this simplified approach, only the mean radius of the population of precipitates is calculated, instead of a distribution of sizes. This approach helps keeping the code simple and easier to understand.
Further explainations about the theoretical principles of the model are presented in the file [doc.pdf](doc.pdf).
PreciPyth was designed for the study of core-shell precipitates in aluminium alloys, but can be adaptated to other systems with some modifications.


#### Ecosystem

**PreciPyth** runs on **Python 3** and is therefore working the same on Windows, GNU/Linux and MacOS.
The code was written as part of the Python *ecosystem*, and relies on the [**Scipy Library**](https://www.scipy.org/) for all numerical solving. More specifically, SciPy modules allowed the use of state-of-the-art numerical integration methods : for [solving differential equations](https://en.wikipedia.org/wiki/Numerical_methods_for_ordinary_differential_equations), thanks to the [scipy.integrate.solve_ivp](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html); root finding was performed by [scipy.optimize.brentq](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.brentq.html) using Brent's method; and scalar functions bounded minimization uses [scipy.optimize.minimize_scalar](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize_scalar.html).

These modules provide rock-solid implementations of advanced numerical methods, allowing to focus on the physics instead of solving algorithms.


## Structure of the project
**PreciPyth** contains 4 important files :

+ `config_file.py` is the only one that has to be edited for common use of the software. It creates a dictionary which contains all user inputs (composition, temperature, method to use, routines to execute, thermodynamic constants,...). 3 routines are available, allowing *isothermal* treatments (e.g. 1 year at 450°C), *multi-step heat treatments* (e.g. 1h at 200°C followed by 1h at 300°C) and creation of *TTP diagrams* (Time Temperature Precipitation) by running multiple simulations on a range of temperature. All routines can be executed using either of the 2 proposed models.
+ `main.py` contains the `main` method and calls the other functions. This file has to be executed to run the program.
+ `simulation_func.py` is the "engine" of the program and solves the equations of the selected model. The functions of this file are meant to be called by `main.py`.
+ `plot_results.py` contains all necessary functions for plotting and is called by `main.py`.

The other files of the project contain methods called by these 3 files, and were separated for the sake of readability. More details can be found on the header of each of these files or methods.

## Inputs and outputs
+ **PreciPyth** takes as **inputs** both thermodynamic and kinetic data, such as surface energy, solubility product, heat treatment, lattice parameter, concentration, diffusion coefficients, ...
+ **Outputs** parameters are the radius, number and composition of precipitates and the composition of the matrix. It will take the form of pdf graphs or raw text data depending on user's choice. It can be set with the booleans "SaveGraph" and "SaveData".

## Examples
> Note : these examples are not up-to-date, and only provide an example of what can be done.
Typical output for an isothermal heat treatment :

![alt text](Examples/example.png)

Typical TTP diagram :

![alt text](Examples/TTT.png)

## Quick start guide

+ The repo can be cloned with the following command ([git](https://git-scm.com/) must be installed):
 
```
git clone https://gitlab.com/Arnall/PreciPyth.git
```

It can also be downloaded from Gitlab's web interface (cloud button on the top).

Installing a [linux shell for Windows 10](https://docs.microsoft.com/en-us/windows/wsl/about) can be useful for Windows users.

+ Start modifying the values in `config_file.py` to try things ! Only modify the other files if you know what you're doing.😏

+ Run the program with your IDE or with :
```
python3 main.py
```

## Limits of the model ?

Some features are not available yet :

- [**Ostwald ripening**](https://en.wikipedia.org/wiki/Ostwald_ripening) (sometimes referred as **coarsening**) of precipitates is not implemented. Coarsening has been [implemented in mean radius models](http://michel.perez.net.free.fr/Perez08.pdf) in homogeneous cases, but core-shell precipitates require a specific approach, since we need to keep track of the composition profile in precipitates. **Dissolution** raises the same issue.
- Ramps of temperature are generally harder to describe for mean radius models than isothermals. For this reason, such treatments were not implemented.


## Ressources

+ Python official tutorial [for beginners](https://wiki.python.org/moin/BeginnersGuide) and [for developpers](https://devguide.python.org/).
+ [Git : the simple Guide](http://rogerdudler.github.io/git-guide/).