# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import pdb
from pdb import set_trace as bp
import math
import os
import datetime
import time
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy import optimize

import simulation_func
kB = 1.38064852E-23 # Boltzmann

def checks(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max):
    CSc_Matrix = Z[2] / (1 - alloy.ALPHA_SC * Z[4]) / alloy.N_0
    CZr_Matrix = Z[3] / (1 - alloy.ALPHA_ZR * Z[4]) / alloy.N_0
    CAl_Matrix = 1 - CSc_Matrix - CZr_Matrix
    CSc_Interface = K_AlSc/pow(alloy.CAl_Matrix,3)*np.exp(2*alloy.Surface_Energy_Sc/(Z[0]*kB*Temperature*alloy.N_0))
    CZr_Interface = K_AlZr/pow(alloy.CAl_Matrix,3)*np.exp(2*alloy.Surface_Energy_Zr/(Z[0]*kB*Temperature*alloy.N_0))
    return [CSc_Matrix, CZr_Matrix, CSc_Interface, CZr_Interface]

def checkStartCore(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max): 
    checkRes = checks(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max)
    return checkRes[0] - 0.80 * alloy.CSc_Matrix

def checkStartShell(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max): 
    checkRes = checks(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max)
    return checkRes[1] - 0.80 * alloy.CZr_Matrix

def checkEndCore(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max): 
    if t>1E-3:
        checkRes = checks(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max)
        return checkRes[0] - checkRes[2] - 1E-2 * checkRes[2]
    else :
        return 1

def checkEndShell(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max): 
    if t>1E-3:
        checkRes = checks(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max)
        return checkRes[1] - checkRes[3] - 1E-2 * checkRes[3]
    else :
        return 1