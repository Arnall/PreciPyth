# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import pdb
from pdb import set_trace as bp
import math
import os
import datetime
import time
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy import optimize

import simulation_func
kB = 1.38064852E-23 # Boltzmann

def growth_simple(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix):
    '''
    Binary model.
    Calculates the growth rate of each element separately. The composition chi_g is the ratio of the growth rates.
    '''

    CSc_Precipitate = 0.25
    CZr_Precipitate = 0.25
    # x = [C_interface_Sc, C_interface_Zr]
    x = [K_AlSc * np.exp(2*alloy.Surface_Energy_Sc/(Z[0]*kB*Temperature*alloy.N_0)),
        K_AlZr * np.exp(2*alloy.Surface_Energy_Zr/(Z[0]*kB*Temperature*alloy.N_0))]
    dR_dt_Sc = DiffSc/Z[0]*(CSc_Matrix - x[0]) / (alloy.ALPHA_SC*CSc_Precipitate - x[0])
    dR_dt_Zr = DiffZr/Z[0]*(CZr_Matrix - x[1]) / (alloy.ALPHA_ZR*CZr_Precipitate - x[1])
    
    if dR_dt_Sc >= 0 and dR_dt_Zr >= 0:
        dR_dt = dR_dt_Sc + dR_dt_Zr 
        chi_g = dR_dt_Zr / (dR_dt_Zr + dR_dt_Sc)
    elif dR_dt_Sc < 0 :
        dR_dt = dR_dt_Zr
        chi_g = 1
    elif dR_dt_Zr < 0 :
        dR_dt = dR_dt_Sc
        chi_g = 0

    return [dR_dt, chi_g]

def nucleation_simple(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix):
    '''
    Binary model.
    Calculates the number of newly formed nuclei dN_dt[] for the different elements, considered separately. 
    The average composition of the nuclei chi_n is the ratio of each dN_dt.
    '''

    CSc_Precipitate = 0.25
    CZr_Precipitate = 0.25
    CSc_Matrix = Z[2] / (1 - alloy.ALPHA_SC * Z[4]) / alloy.N_0
    CZr_Matrix = Z[3] / (1 - alloy.ALPHA_ZR * Z[4]) / alloy.N_0
    CAl_Matrix = 1 - CSc_Matrix - CZr_Matrix
    try : 
        dg_Sc = -kB * Temperature * alloy.N_0 * (3*np.log(CAl_Matrix) + np.log(CSc_Matrix) - np.log(K_AlSc))
        dg_Zr = -kB * Temperature * alloy.N_0 * (3*np.log(CAl_Matrix) + np.log(CZr_Matrix) - np.log(K_AlZr))
    except FloatingPointError:
        bp()
    R_Star_Sc = -2.*alloy.Surface_Energy_Sc/dg_Sc+0.5*pow(kB*Temperature/np.pi/alloy.Surface_Energy_Sc,0.5)
    R_Star_Zr = -2.*alloy.Surface_Energy_Zr/dg_Zr+0.5*pow(kB*Temperature/np.pi/alloy.Surface_Energy_Zr,0.5)
    dG_Star_Sc = 16/3*np.pi*pow(alloy.Surface_Energy_Sc,3)/pow(dg_Sc,2)
    dG_Star_Zr = 16/3*np.pi*pow(alloy.Surface_Energy_Zr,3)/pow(dg_Zr,2)
    beta_Star_Sc = 4 * np.pi/pow(alloy.LATTICE_PARAMETER,4) * pow(R_Star_Sc,2)* DiffSc * CSc_Matrix
    beta_Star_Zr= 4 * np.pi/pow(alloy.LATTICE_PARAMETER,4) * pow(R_Star_Zr,2)* DiffZr * CZr_Matrix
    zeldovich_Sc = alloy.Vat_Al3Sc / (2*np.pi*R_Star_Sc**2) * pow(alloy.Surface_Energy_Sc/kB/Temperature,0.5)
    zeldovich_Zr = alloy.Vat_Al3Zr / (2*np.pi*R_Star_Zr**2) * pow(alloy.Surface_Energy_Zr/kB/Temperature,0.5)
    
    tau_Sc = 1/(2*np.pi*beta_Star_Sc*zeldovich_Sc*zeldovich_Sc)
    tau_Zr = 1/(2*np.pi*beta_Star_Zr*zeldovich_Zr*zeldovich_Zr)
    # tau = [tau_Sc, tau_Zr]
    dN_dt_Sc = alloy.N_0 * zeldovich_Sc * beta_Star_Sc * np.exp(-dG_Star_Sc/kB/Temperature) * (1-np.exp(-t/tau_Sc))
    
    ## Disable Zr nucleation :
    # dN_dt_Zr = 0
    # chi_nucleation = 0
    ## Enable Zr nucleation :
    dN_dt_Zr = alloy.N_0 * zeldovich_Zr * beta_Star_Zr * np.exp(-dG_Star_Zr/kB/Temperature) * (1-np.exp(-t/tau_Zr))
    if dN_dt_Sc >= 0 and dN_dt_Zr >= 0:
        dN_dt = dN_dt_Sc + dN_dt_Zr 
        chi_n = dN_dt_Zr / (dN_dt_Zr + dN_dt_Sc)
    elif dN_dt_Sc < 0 :
        dN_dt = dN_dt_Zr
        chi_n = 1
    elif dN_dt_Zr < 0 :
        dN_dt = dN_dt_Sc
        chi_n = 0
    # try :
    #     chi_nucleation = dN_dt_Zr / (dN_dt_Sc + dN_dt_Zr)
    # except : 
    #     chi_nucleation = 0
    return [dN_dt_Sc, dN_dt_Zr], chi_n, [R_Star_Sc, R_Star_Zr]