# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import pdb
import math
import os
import datetime
import time
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

import simulation_func
import plot_results

kB = 1.38064852E-23 # Boltzmann

class AlScZr:
    """For describing properties of an Al-Sc-Zr alloy.
    This object will be created using the inputs given in `config_file.py`.

    """

    def __init__(self, conditions):
        self.CSc_Matrix = conditions["CSc_Matrix_initial"]
        self.CZr_Matrix = conditions["CZr_Matrix_initial"]
        self.CAl_Matrix = 1 - self.CZr_Matrix - self.CSc_Matrix

        self.Surface_Energy_Sc = conditions["Surface_Energy_Sc"]
        self.Surface_Energy_Zr = conditions["Surface_Energy_Zr"]

        # Crystallographic parameters
        self.ALPHA_SC = conditions["ALPHA_SC"] # V_atMatrix/V_atAl3Sc = (a_mat^3)/4 / (a_Al3sc^3)/4 
        self.ALPHA_ZR = conditions["ALPHA_ZR"] # V_Al/V_Zr
        self.Vat_Al3Sc = conditions["Vat_Al3Sc"]
        self.Vat_Al3Zr = conditions["Vat_Al3Zr"]
        self.LATTICE_PARAMETER = conditions["LATTICE_PARAMETER"] # LP of the aluminium matrix 

        # Thermo-kinetic parameters
        self.PRE_FACTOR_DIFF_SC = conditions["PRE_FACTOR_DIFF_SC"]
        self.PRE_FACTOR_DIFF_ZR = conditions["PRE_FACTOR_DIFF_ZR"]
        self.BARRIER_DIFF_SC = conditions["BARRIER_DIFF_SC"]
        self.BARRIER_DIFF_ZR = conditions["BARRIER_DIFF_ZR"]
        self.N_0 = 4/pow(self.LATTICE_PARAMETER,3) # number of nucleation sites per square meter = 1/Vat = 1/(a^3/4) in a fcc
        self.solubility_A_Sc = conditions["solubility_A_Sc"]
        self.solubility_B_Sc = conditions["solubility_B_Sc"]
        self.solubility_A_Zr = conditions ["solubility_A_Zr"]
        self.solubility_B_Zr = conditions["solubility_B_Zr"]
        

        """How to store "chi" ?
        For results analysis, it is useful to store the values of chi along time. 
        These values can't be returned by default using the solver (scipy.integrate.solve_ivp).
        The hack used here is to store it on-the-fly in a table, which is an attribute of AlScZr.
        The main drawback of this approach is the need to use dynamical allocation (keyword "append")
        at each time-step, since it's impossible to predict how many time-steps the solver will use.
        The performance change is not noticeable though.

        One table is used for "chi nucleation" and called chi_n_hist. A similar one is dedicated to 
        growth, and called chi_g_hist. The structure of the tables is : 
        chi_n_hist = [[chi_n(t1), chi_n(t2), ..],[t1, t2, ..]]
        chi_g_hist = [[chi_g(t1), chi_g(t2), ..],[t1, t2, ..]]
        """
        self.chi_n_hist = [[],[]]
        self.chi_g_hist = [[],[]]

    def GetDiffSc(self,Temperature):
        return self.PRE_FACTOR_DIFF_SC*np.exp(-self.BARRIER_DIFF_SC/8.314/Temperature)

    def GetDiffZr(self,Temperature):
        return self.PRE_FACTOR_DIFF_ZR*np.exp(-self.BARRIER_DIFF_ZR/8.314/Temperature)

    def GetK_AlSc(self,Temperature):
        return pow(10,-self.solubility_A_Sc/Temperature+self.solubility_B_Sc)

    def GetK_AlZr(self,Temperature):
        return pow(10,-self.solubility_A_Zr/Temperature+self.solubility_B_Zr)