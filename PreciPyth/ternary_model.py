# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import pdb
from pdb import set_trace as bp
import math
import os
import datetime
import time
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy import optimize

import simulation_func
import binary_model
kB = 1.38064852E-23 # Boltzmann


def growth_chi(chi_g, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix):
    '''Returns dR/dt as a function of the composition chi.'''

    # x = X_interface
    with np.errstate(divide='ignore'):
        # np.errstate is used to get rid of an annoying warning
        x = [(1-chi_g) * K_AlSc * np.exp(2*alloy.Surface_Energy_Sc/(Z[0]*kB*Temperature*alloy.N_0)),
            chi_g * K_AlZr * np.exp(2*alloy.Surface_Energy_Zr/(Z[0]*kB*Temperature*alloy.N_0))]
        return DiffSc*(CSc_Matrix - x[0])/(alloy.ALPHA_SC*(1-chi_g)/4. - x[0]) - DiffZr*(CZr_Matrix - x[1])/(alloy.ALPHA_ZR*chi_g/4. - x[1])

def dNdt_chi(chi_n, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t):
    '''Returns dN/dt as a function of the composition chi.'''

    CSc_Precipitate = 0.25
    CZr_Precipitate = 0.25
    CSc_Matrix = Z[2] / (1 - alloy.ALPHA_SC * Z[4]) / alloy.N_0
    CZr_Matrix = Z[3] / (1 - alloy.ALPHA_ZR * Z[4]) / alloy.N_0
    CAl_Matrix = 1 - CSc_Matrix - CZr_Matrix

    if chi_n>1:
        chi_n=1
    if chi_n<0:
        chi_n=0
    
    dg = -kB*Temperature*alloy.N_0*np.log((CAl_Matrix**3*CZr_Matrix)**chi_n \
            *(CAl_Matrix**3*CSc_Matrix)**(1-chi_n)/((chi_n*K_AlZr)**chi_n \
                * ((1-chi_n)*K_AlSc)**(1-chi_n) ))
            

    Surface_Energy = chi_n * alloy.Surface_Energy_Zr + (1-chi_n) * alloy.Surface_Energy_Sc
    R_Star = -2.*Surface_Energy/dg+0.5*pow(kB*Temperature/np.pi/Surface_Energy,0.5)
    assert R_Star >= 0, "Invalid value of R_Star : {}".format(CSc_Matrix)
    
    dG_Star = 16/3*np.pi*pow(Surface_Energy,3)/pow(dg,2)
    beta_Star = 4 * np.pi/pow(alloy.LATTICE_PARAMETER,4) * pow(R_Star,2) \
        / (chi_n / (DiffZr*CZr_Matrix) + (1-chi_n) / (DiffSc*CSc_Matrix))
    Vat = alloy.Vat_Al3Sc * (1-chi_n) + alloy.Vat_Al3Zr * chi_n
    zeldovich = Vat / (2*np.pi*R_Star**2) * pow(Surface_Energy/kB/Temperature,0.5)
    
    tau = 1/(2*np.pi*beta_Star*zeldovich**2)
    
    # calculate -dN/dt
    dN_dt = - alloy.N_0 * zeldovich * beta_Star * np.exp(-dG_Star/kB/Temperature) * (1-np.exp(-t/tau))

    return dN_dt

def growth_system(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix):
    '''Ternary model.
    Calculates the growth rate by solving dR/dt_Sc(chi_g) = dR/dt_Zr(chi_g).
    '''

    CSc_Precipitate = 0.25
    CZr_Precipitate = 0.25
    chi_g = optimize.brentq(f=growth_chi, a = 0, b = 1,
         args= (Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix))
    
    CSc_Interface = (1-chi_g) * K_AlSc * np.exp(2*alloy.Surface_Energy_Sc/(Z[0]*kB*Temperature*alloy.N_0))
    CZr_Interface = chi_g * K_AlZr * np.exp(2*alloy.Surface_Energy_Zr/(Z[0]*kB*Temperature*alloy.N_0))
    CAl_Interface = 1- CSc_Interface - CZr_Interface
    dR_dt = DiffSc/Z[0]*(CSc_Matrix - CSc_Interface) / ((1-chi_g)*alloy.ALPHA_SC*CSc_Precipitate - CSc_Interface)
    if np.isinf(dR_dt) or np.isnan(dR_dt):
        dR_dt = DiffZr/Z[0]*(CZr_Matrix - CZr_Interface) / ((chi_g)*alloy.ALPHA_ZR*CZr_Precipitate - CZr_Interface)
    
    # if t> 1E2*3600:
    #     # bp()
    #     chi_vec = np.arange(0,1,1E-2)
    #     drdt_vec = [growth_chi(chi, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix) for chi in chis]
    #     plt.plot (chi_vec, drdt_vec)
    #     plt.show()

    return [dR_dt, chi_g]

def nucleation_maximize(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix):
    '''Ternary model.
    Calculates the number of newly formed nuclei dN_dt, and their composition chi_n by maximizing dN_dt(chi_n).
    '''


    #### Plot dN/dt --- To be removed
    # space = np.arange(0,1,1E-4)
    # for Temperature in range(250, 600, 50):
    #     DiffSc = alloy.GetDiffSc(Temperature)
    #     DiffZr = alloy.GetDiffZr(Temperature)
    #     K_AlSc = alloy.GetK_AlSc(Temperature)
    #     K_AlZr = alloy.GetK_AlZr(Temperature)
    #     plt.semilogy(space,-dNdt_chi(space, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t), label = 'dN_dt '+str(Temperature))
    # plt.legend(loc='best')
    # plt.xlim(0,1)
    # plt.show()
  
    bds = optimize.Bounds(0,1)
    # solu_chi = optimize.minimize_scalar(dNdt_chi, bounds=(0,1), args=(Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t),
    #     method='bounded')
    
    # solu_chi = optimize.brute(dNdt_chi,ranges=((0, 1),), Ns=50, full_output=True, args=(Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t))
    # chi_n = solu_chi[0]
    # dN_dt = -solu_chi[1]
    # x0 = 0
    x0 = binary_model.nucleation_simple(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix)[1]
    try:
        solu_chi = optimize.minimize(dNdt_chi, [x0], bounds=bds, method='TNC', args=(Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t))
    except AssertionError:
        chi_n = alloy.chi_n_hist[0][-1]
        dN_dt = 0
        R_Star = 1E-15
    else:
        chi_n = solu_chi.x[0]
        dN_dt = - solu_chi.fun[0]
    
        dg = -kB*Temperature*alloy.N_0*np.log((CAl_Matrix**3*CZr_Matrix)**chi_n \
                *(CAl_Matrix**3*CSc_Matrix)**(1-chi_n)/((chi_n*K_AlZr)**chi_n \
                    * ((1-chi_n)*K_AlSc)**(1-chi_n) ))
        Surface_Energy = chi_n * alloy.Surface_Energy_Zr + (1-chi_n) * alloy.Surface_Energy_Sc
        R_Star = -2.*Surface_Energy/dg+0.5*pow(kB*Temperature/np.pi/Surface_Energy,0.5)

    return dN_dt, chi_n, [R_Star]