# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import pdb
import math
import os
import datetime
import time
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

import simulation_func
import plot_results
import physical_system
import config_file

kB = 1.38064852E-23 # Boltzmann

def main():
    '''Runs the routines uncommented by the user in `config_file.py.`
    '''

    # Clear terminal (cross-platform)
    os.system('cls' if os.name == 'nt' else 'clear')
    
    conditions = config_file.inputs()

    # Runs all not-commented methods.
    if 'Run_isothermal' in conditions:
        isothermal(conditions)

    if 'Temperature_range' in conditions:
        diagTTT(conditions)
   
    if 'steps' in conditions:
        multistep(conditions)

def generateExperimentID(alloy, conditions):
    '''Generates the title and filename of the graphs.
    '''
    t_max = conditions["t_max"]
    # Convert time to a readable format
    if t_max > 3600*24*365:
        if t_max > 1000*3600*24*365:
            duration = r"$10^{"+str(int(np.log10(t_max / 3600. / 24 / 365))) + "}$ yrs"
        else:
            duration = str(t_max / 3600. / 24 / 365) + " yrs"
    else :
        duration = datetime.timedelta(seconds=t_max)
    
    if 'steps' in conditions :
        title = "Al-"+str(alloy.CSc_Matrix*100)+"at% Sc-"+str(alloy.CZr_Matrix*100)+"at% Zr alloy. - Multistep treatment. ƔSc : "+\
        str(alloy.Surface_Energy_Sc) + ", ƔZr : " + str(alloy.Surface_Energy_Zr)+" J/m²."
    else:
        title = "Al-"+str(alloy.CSc_Matrix*100)+"at% Sc-"+str(alloy.CZr_Matrix*100)+"at% Zr alloy. \n Isothermal treatment "+\
            str(duration)+" at "+str(conditions["Temperature"]-273)+"°C. ƔSc : "+ str(alloy.Surface_Energy_Sc)+\
            ", ƔZr : "+str(alloy.Surface_Energy_Zr)+" J/m²."
    
    if 'steps' in conditions :
        short_title = str(alloy.CSc_Matrix*100)+"_"+str(alloy.CZr_Matrix*100)+"_"+str(conditions["Temperature"]-273)+"_"+str(t_max)+"_"+\
            str(alloy.Surface_Energy_Sc)+str(alloy.Surface_Energy_Zr)        
    else :
        short_title = str(alloy.CSc_Matrix*100)+"_"+str(alloy.CZr_Matrix*100)+"_"+str(conditions["Temperature"]-273)+"_"+str(t_max)+"_"+\
            str(alloy.Surface_Energy_Sc)+str(alloy.Surface_Energy_Zr)
    
    print("Isothermal heat treatment.\nParameters : \nCSc_Matrix_initial (at%) : "+str(alloy.CSc_Matrix*100)+"\nCZr_Matrix_initial (at%) : "+\
        str(alloy.CZr_Matrix*100)+"\nTime initial : "+str(conditions["t_min"])+"\nTmax : "+str(duration)+\
        "\nTemperature : "+str(conditions["Temperature"]-273)+"\n")

    return (title, short_title, conditions["saveGraph"], conditions["showGraph"], conditions["saveData"])

def multistep(conditions):
    # structure : steps = [[time1, Temperature1], [time2, Temperature2], ...]
    steps = conditions["steps"]
    alloy =  physical_system.AlScZr(conditions)
    
    conditions["t_max"] = 0
    for step in steps : conditions["t_max"] += step[0] # calculate total t_max as a sum of each step duration
    
    experimentID = generateExperimentID(alloy, conditions)
    results = []

    for method_id, method in enumerate(conditions["methods"]):
        t_min = conditions["t_min"]
        t_max = 0
        Z0 = []
        
        for stp_id, step in enumerate(steps):
            conditions["Temperature"] = step[1] + 273.
            t_max = t_max + step[0]
            
            if stp_id > 0: 
                t_min = round(result.t[-1])
                
                Z0 = np.zeros(5)
                for i in range(5):
                    Z0[i] = result.y[i][-1] # final conditions become initial conditions
                
                t_points = np.logspace(np.log10(t_min), np.log10(t_max), num = 3E2) # new list of times to eval
                t_points[-1] = round(t_points[len(t_points)-1]) # because 10**log10(x) can be slightly bigger than x
                if t_points[0]>=1:
                    t_points[0] = round(t_points[0]) # avoids a numerical rounding bug

                temp_result = simulation_func.simulation(alloy, conditions, t_points, Z0, method)
                
                if temp_result.success :
                    result.y = np.append(result.y, temp_result.y, axis=1)
                    result.t = np.append(result.t, temp_result.t, axis=0)
                else :
                    print("Step {} failed for method {}".format(step, method))
            
            else :
                t_points = np.logspace(np.log10(t_min), np.log10(t_max), num = 3E2)
                result = simulation_func.simulation(alloy, conditions, t_points, Z0, method)
        
        dict_res = {
            "method" : method,
            "result" : result,
            "chi_n_values" : alloy.chi_n_hist[0],
            "chi_n_times" : alloy.chi_n_hist[1],
            "chi_g_values" : alloy.chi_g_hist[0],
            "chi_g_times" : alloy.chi_g_hist[1]
            }
        results.append(dict_res) # 1 dictionary is added for each method
        alloy = physical_system.AlScZr(conditions)

    # Plot
    plot_results.curves(results, experimentID, conditions)

def isothermal(conditions):
    Z0 = []
   
    if "Temperature" not in conditions:
        conditions["Temperature"] = float(input('Temperature (°C) ? ')) + 273.

    alloy =  physical_system.AlScZr(conditions)
    experimentID = generateExperimentID(alloy, conditions)
    t_points = np.logspace(np.log10(conditions["t_min"]), np.log10(conditions["t_max"]), num = 3E2)
    t_points[len(t_points)-1] = round(t_points[len(t_points)-1])

    start_time = time.time()

    results = []
    # structure is [[solutions],[chi_n_hist, t],[chi_g_hist, t]]
    # try :
    for method in conditions["methods"]:
        dict_res = {
        "method" : method,
        "result" : simulation_func.simulation(alloy, conditions, t_points, Z0, method),
        "chi_n_values" : alloy.chi_n_hist[0],
        "chi_n_times" : alloy.chi_n_hist[1],
        "chi_g_values" : alloy.chi_g_hist[0],
        "chi_g_times" : alloy.chi_g_hist[1]
        }
        results.append(dict_res) # 1 dictionary is added for each method
        
        print('Method : '+method[0]+', '+method[1]+'\n' + dict_res["result"].message + "\n Nb of events : " + str(len(dict_res["result"].t_events))) 
        alloy =  physical_system.AlScZr(conditions)
    
    print("\n--- Computation time :  %s seconds ---" % (time.time() - start_time))
    
    # except :
    #     print("\n-- Resolution failed --")
    
    # finally:
    print("\nPlotting results... ", end='')
        
        # try :
    plot_results.curves(results, experimentID, conditions)
        
        # except:
        #     print("Plotting failed")

def diagTTT(conditions):    
    Temperature_range = conditions["Temperature_range"]
    nbsteps = len(np.arange(Temperature_range[0], Temperature_range[1], Temperature_range[2]))
    radius = np.zeros(nbsteps)
    number = np.zeros(nbsteps)
    temperature = np.zeros(nbsteps)
    i = 0
    TTT = np.zeros((nbsteps,8))

    for Temperature in range(Temperature_range[0] + 273, Temperature_range[1] + 273, Temperature_range[2]):
        alloy =  physical_system.AlScZr(conditions)
        # experimentID = generateExperimentID(alloy, Temperature, t_min, t_max, False, False, False)
        Z0 = []
        conditions["Temperature"] = Temperature
        workdone=i/nbsteps
        print("\r\033[1m    Progress:\033[0;0m [{0:50s}] {1:.1f}%".format(u"\u2588" * int(workdone * 50), workdone*100), end="", flush=True)
        result = simulation_func.simulation(alloy,  conditions, [conditions["t_max"]], Z0, conditions["methods"][0])
        # TTT[temp] = [Temperature-273, fv_max, t_start_core, t_end_core, t_start_shell, t_end_shell, radius, number]
        fv_max = 4*np.pi / 3 * result.y[0][0] * result.y[0][0] * result.y[0][0] * result.y[1][0]
        if fv_max > 0.1/100:
            try :
                TTT[i] = [Temperature-273, fv_max,\
                result.t_events[0], result.t_events[1][0], result.t_events[2][0], result.t_events[3][0], result.y[0][0], result.y[1][0]]
            except IndexError:
                print("\nError : At least 1 missing event for temperature "+str(Temperature-273))
                TTT[i] = [Temperature-273, fv_max,\
                result.t_events[0], result.t_events[1][0], result.t_events[2][0], None, result.y[0][0], result.y[1][0]]
        i += 1
    
    try:
        title = str(conditions["CSc_Matrix_initial"])+"_"+str(conditions["CZr_Matrix_initial"])+"_"+str(Temperature_range[0])\
            +"_"+str(Temperature_range[1])+"_"+str(conditions["Surface_Energy_Sc"])+"_"+str(conditions["Surface_Energy_Zr"])

        if os.path.isfile(title+".txt"):
            os.remove(title+".txt")
        np.savetxt('TTT_Diags/'+title+".txt", TTT, delimiter=';')
    except :
        print("Could not save :(")
    else : 
        print("\nSave : done !")
    plot_results.plotTTT(TTT, Temperature_range[0], Temperature_range[1], conditions, title)

main()