# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import pdb
from pdb import set_trace as bp
# import math
# import os
# # import datetime
# import time
# import matplotlib.pyplot as plt
# from scipy.integrate import solve_ivp
# from scipy import optimize

import _ivp
import precipitation_events
import binary_model
import ternary_model

kB = 1.38064852E-23 # Boltzmann

def model(t, Z, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max, method):
    '''Calculates dZdt from a value of (t, Z). It's the right-hand side of the system, put in a vector-like structure.
    Z = [R, N, n_Sc, n_Zr, fv]
    '''

    CSc_Precipitate = 0.25
    CZr_Precipitate = 0.25

    # Calculate volume fraction and concentration from R, N, n
    CSc_Matrix = Z[2] / (1 - alloy.ALPHA_SC * Z[4]) / alloy.N_0
    CZr_Matrix = Z[3] / (1 - alloy.ALPHA_ZR * Z[4]) / alloy.N_0
    CAl_Matrix = 1 - CSc_Matrix - CZr_Matrix

    # Make sure the compositions make sense :
    assert 1 >= CSc_Matrix >= 0, "Invalid value of CSc_Matrix : {}".format(CSc_Matrix)
    assert 1 >= CZr_Matrix >= 0, "Invalid value of CZr_Matrix : {}".format(CZr_Matrix) 
    assert 1 >= CAl_Matrix >= 0, "Invalid value of CAl_Matrix : {}".format(CAl_Matrix)

    ##### Nucleation #####
    if method[0] == 'nuc_simp' :
        dN_dt, chi_nucleation, R_Star = binary_model.nucleation_simple(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix)
        if dN_dt == 0 or np.isnan(chi_nucleation):
            # dN_dt = 0 may result in chi_nucleation = NaN
            chi_nucleation = 0

        # mass balance
        dnSc_dt = - dN_dt[0] * CSc_Precipitate / alloy.Vat_Al3Sc * 4*np.pi*R_Star[0]**3/3
        dnZr_dt = - dN_dt[1] * CZr_Precipitate / alloy.Vat_Al3Zr * 4*np.pi*R_Star[1]**3/3
        dN_dt = dN_dt[0] + dN_dt[1]

    elif method[0] == 'nuc_mini':
        dN_dt, chi_nucleation, R_Star = ternary_model.nucleation_maximize(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix)

        # mass balance
        dnSc_dt = - (1-chi_nucleation) * dN_dt * CSc_Precipitate / alloy.Vat_Al3Sc * 4*np.pi*(R_Star[0]**3/3)
        dnZr_dt = - chi_nucleation * dN_dt * CZr_Precipitate / alloy.Vat_Al3Zr * 4*np.pi*(R_Star[-1]**3/3)
    
    else :
        raise ValueError("Unknown method for nucleation.")

    # Save the value of chi_n (in an inelegant way)
    if chi_nucleation > 0:
        alloy.chi_n_hist[0].append(chi_nucleation)
        alloy.chi_n_hist[1].append(t)

    ####### Growth ######
    if method[1] == 'gr_simp' :
        dR_dt, chi_g = binary_model.growth_simple(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix)
    elif method[1] == 'gr_sys':
        dR_dt, chi_g = ternary_model.growth_system(Z, t, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, CSc_Matrix, CZr_Matrix, CAl_Matrix)
    else :
        raise ValueError("Unknown method for growth.")
    
    if np.isnan(dR_dt):
    	# a workaround for integration errors when dR_dt approx. = 0
        dR_dt = 0

    # Save the value of chi_n (in an inelegant way)
    if chi_g > 0:
        alloy.chi_g_hist[0].append(chi_g)
        alloy.chi_g_hist[1].append(t)
    
    ###### Mass balance for Growth
    dnSc_dt_g = - 4 * np.pi * Z[0] * Z[0] * (1 - chi_g) / 4 * Z[1] / alloy.Vat_Al3Sc * dR_dt
    dnZr_dt_g = - 4 * np.pi * Z[0] * Z[0] * chi_g / 4 * Z[1] / alloy.Vat_Al3Zr * dR_dt

    ###### calculate volume fraction and diminution of mean radius due to nucleation
    R_Star_chi = (R_Star[0] *(1-chi_nucleation)+ R_Star[-1]*chi_nucleation)
    dfv_dt  = 4*np.pi/3 * (3*dR_dt * Z[0]**2 * Z[1] + dN_dt * R_Star_chi **3)
    dR_dt += dN_dt / Z[1] * (R_Star_chi - Z[0]) # Mean radius evolution

    print("%.4f" % (t/3600) + " h", end='\r')
    return [dR_dt, dN_dt, dnSc_dt+dnSc_dt_g, dnZr_dt+dnZr_dt_g, dfv_dt]

def simulation(alloy, conditions, t_points, Z0, method):
    '''Prepares the simulation and calls the solver. Returns the result of solve_ivp.
    '''
    Temperature = conditions["Temperature"]
    t_max = conditions["t_max"]

    DiffSc = alloy.GetDiffSc(Temperature)
    DiffZr = alloy.GetDiffZr(Temperature)
    K_AlSc = alloy.GetK_AlSc(Temperature)
    K_AlZr = alloy.GetK_AlZr(Temperature)
    CAl_Matrix = alloy.CAl_Matrix
    CSc_Matrix = alloy.CSc_Matrix
    CZr_Matrix = alloy.CZr_Matrix
    t = conditions["t_min"]
    n_Sc = CSc_Matrix * alloy.N_0
    n_Zr = CZr_Matrix * alloy.N_0
    
    n_init = conditions["n_init"]
    r_init = conditions["r_init"]
    
    n_Sc -= n_init * 0.25 / alloy.Vat_Al3Sc * 4*np.pi* r_init**3 /3

    if Z0 == []:
            Z0 = np.array([r_init, n_init, n_Sc, n_Zr, 4/3*np.pi * r_init**3 * n_init])
    
    # precipitation_events.checkStartCore.direction = -1
    # precipitation_events.checkStartShell.direction = -1
    # precipitation_events.checkEndCore.direction = -1
    # precipitation_events.checkEndShell.direction = -1
    # precipitation_events.checkEndShell.terminal = True
    
    # Call to the function solve_ivp from the Scipy library for solving the system of ODEs.
    # The "lambda t,y: myfunction(t, y, arg_1, arg_2, ...)" structure is just a trick to pass 
    # more than just t and y as arguments. 
    # We have to use it because solve_ivp needs functions with signature func(t,y).
    # BDF : rtol = 1E-6, atol = 1E-12
    # try :
    solution = _ivp.solve_ivp(fun=lambda t, y: model(t, y, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max, method),
         t_span = [conditions["t_min"],t_max], method = 'BDF', y0 = Z0, rtol= 1E-6, dense_output = True, atol = 1E-12, t_eval = t_points,
         
         events=[lambda t, y: precipitation_events.checkStartCore(t, y, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max),
         lambda t, y: precipitation_events.checkEndCore(t, y, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max),
         lambda t, y: precipitation_events.checkStartShell(t, y, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max),
         lambda t, y: precipitation_events.checkEndShell(t, y, alloy, Temperature, DiffSc, DiffZr, K_AlSc, K_AlZr, t_max)])
    return solution
    # except : 
    #     print("Resolution failed at temperature = "+str(Temperature-273)+"°C")
    #     return 