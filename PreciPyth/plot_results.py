# Mean radius model of core-shell precipitates in AlScZr alloys
# Copyright (C) June 2018
# Arnaud Allera, Baptiste Rouxel, Michel Perez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pdb
import math
import os
from scipy.interpolate import UnivariateSpline  
import pandas as pd
from pdb import set_trace as st

margin_xmax = 1.5

def curves(results, experiment, conditions):
    title, short_title, saveGraph, showGraph, saveData = experiment

    # if not os.path.exists("graphs"):
    #     os.makedirs("graphs")
    # plt.semilogx(results[1][0][1],results[1][0][0], label = 'chi_n')
    # plt.semilogx(results[2][0][1],results[2][0][0], label = 'chi_g')
    # plt.legend(loc='best')
    # plt.show()

    params = {'legend.fontsize': 'xx-large',
              'figure.figsize': (15, 12),
             'axes.labelsize': 'xx-large',
             'axes.titlesize':'xx-large',
             'xtick.labelsize':'xx-large',
             'ytick.labelsize':'xx-large',
             'font.family' : 'serif'}
    rcParams.update(params)
    fig = plt.figure()


    plt.subplot(231)
    RadVSTime(results, conditions)

    plt.subplot(232)
    NumberVSTime(results, conditions)

    plt.subplot(233)
    CVSTime(results, conditions) 
       
    plt.subplot(234)
    # fvVSTime(results, conditions)
    ChigVSRad(results, conditions)

    plt.subplot(235)
    ChinVSTime(results,conditions)

    plt.subplot(236)
    ChigVSTime(results,conditions)
    
    # manage the spacing between elements
    plt.tight_layout()
    if conditions["graph_display_title"]:
        fig.suptitle(title, fontsize=16)
        fig.subplots_adjust(top=0.92)

    if saveGraph == True:
        plt.savefig("../graphs/" + short_title +".pdf")

    # Say something
    print("\nGraphs : done !")

    if showGraph == True:
        plt.show()

    if saveData == True:
        # Save data in a file (to be generealized to multi conditions["methods"] !)
        print("Saving...")
        try:
            path = "../graphs/"
            # if os.path.isfile(path+short_title+".txt"):
            #     os.remove(path+short_title+".txt")
            np.savetxt(path+short_title+"-binary.txt", np.column_stack((np.transpose(results[0]["result"].t),np.transpose(results[0]["result"].y))), delimiter=',', newline='\n', header='Time(s);Radius(m);Number(m^-3);n_Sc;n_Zr',fmt='%1.4e')
            np.savetxt(path+short_title+"-ternary.txt", np.column_stack((np.transpose(results[1]["result"].t),np.transpose(results[0]["result"].y))), delimiter=',', newline='\n', header='Time(s);Radius(m);Number(m^-3);n_Sc;n_Zr',fmt='%1.4e')
            # np.savetxt("time_"+short_title+".txt", np.transpose(result.t), delimiter=';', newline=';\n', header='time (s)',fmt='%1.4e')

        except :
            print("Could not save :(")
            # pdb.set_trace()
        else : 
            print("Save : done !")


def RadVSTime(results, conditions) :
    plt.ylabel('Radius (m)')
    plt.xlabel('Time (hours)')
    plt.ylim(5E-10,8E-9)
    plt.xlim(1E-3*0.8,margin_xmax*max(max(results[0]["result"].t/3600),max(results[-1]["result"].t/3600)))
    plt.tick_params(axis='y')
    
    for method_id, dict_res in enumerate(results):
        plt.semilogx(dict_res["result"].t/3600, dict_res["result"].y[0], color='C'+str(method_id), label = conditions["methods_names"][method_id] + ' model')
    
    try :
        df = pd.read_csv(conditions["path_SANS"])
        rad_time_SANS = df[df.Composition == conditions["composition_name"]][['Time (h)','Radius SANS (A)','UncertaintyRadSANS']].values
        label = "SANS"
        for x,y,dy in rad_time_SANS:
            plt.errorbar(x,y*1E-10, yerr=dy*1E-10, label=label, fmt='o', markersize = 7, c='C3')
            label = None
    except:
        pass
    
    try :
        rad_time_TEM = df[df.Composition == conditions["composition_name"]][['Time (h)','Radius TEM (A)','UncertaintyRadTEM']].values
        label = "TEM"
        for x,y,dy in rad_time_TEM:
            plt.errorbar(x,y*1E-10, yerr=dy*1E-10, label=label, fmt='o', markersize = 7, c='C4')
            label = None
    except:
        pass
    plt.legend(loc='best')

    # ax2.legend(loc='lower right')

    # plt2 = plt.loglog(Time,results_array[:,10], label = 'R*',color ='green')
    # plt3 = ax2.loglog(Time,results_array[:,3], color='orange', label=r'$\chi$')

    # a little hack for the legend :
    # plt_legend = plt1 + plt2 + plt3
    # labs = [l.get_label() for l in plt_legend]

def ChigVSRad(results, conditions) :
    plt.xlabel('Radius (m)')
    plt.ylabel(r'Average composition in precipitate (at. %)')
    plt.tick_params(axis='y')
    plt.xlim(right=6E-9)
    plt.ylim(top=35)
    for method_id, dict_res in enumerate(results):

        rad_time_chig = []
        for time_pts in dict_res["chi_g_times"]:
            rad_time_chig.append(dict_res["result"].sol.__call__(time_pts)[0])
        plt.plot(rad_time_chig, [100*(1-x)/4 for x in dict_res["chi_g_values"]], color='C'+str(method_id), label = 'Sc ' + conditions["methods_names"][method_id] + ' model')
        plt.plot(rad_time_chig, [100*(x)/4 for x in dict_res["chi_g_values"]], linestyle="dashed", color='C'+str(method_id), label ='Zr ' + conditions["methods_names"][method_id] + ' model')
    
    try :
        # Plot proxigram, available only for A2 composition, 10 days for the moment
        if conditions["composition_name"] == "A2" and conditions["t_max"] == 10*24*3600:
            df = pd.read_csv(conditions["path_APT"], decimal=",")
            # Extract the columns
            # rad_proxigram = df[['Distance (nm)']].values
            # compo_rad_Sc = df[['Sc %']].values
            # err_compo_rad_Sc = df[['Sc % Error']].values

            # compo_rad_Zr = df[['at pct Zr matrix']].values
            # err_compo_rad_Zr = df[['at pct Zr matrix']].values
            plt.errorbar([x*1E-9*4/3 for x in df[['Distance (nm)']].values], df[['Sc %']].values, yerr=df[['Sc % Error']].values,ecolor = 'gray', errorevery=5, label="Sc (APT)", markersize =2, c='C5')
            plt.errorbar([x*1E-9*4/3 for x in df[['Distance (nm)']].values], df[['Zr %']].values, yerr=df[['Zr % Error']].values,ecolor = 'gray', errorevery=5, label="Zr (APT)", markersize =2, c='C6')
        else :
            print("No APT data available")
    except:
        print("Error importing APT data")
        pass

    plt.legend(loc='center left')
        

def NumberVSTime(results, conditions) :
    plt.tick_params(axis='y')
    plt.xlim(1E-3*0.8,margin_xmax*max(max(results[0]["result"].t/3600),max(results[-1]["result"].t/3600)))
    plt.ylabel(r'Number ($m^{-3}$)')
    plt.xlabel('Time (hours)')

    for method_id, dict_res in enumerate(results):
        plt.loglog(dict_res["result"].t/3600, dict_res["result"].y[1], color='C'+str(method_id), label = conditions["methods_names"][method_id] + ' model')

    try :
        df = pd.read_csv(conditions["path_SANS"])
        df = df.dropna(subset=["NumberTEM (m^-3)"])
        num_time_TEM = df[df.Composition == conditions["composition_name"]][['Time (h)','NumberTEM (m^-3)']].values
        label = "TEM"
        for x,y in num_time_TEM:
            plt.errorbar(x, y, yerr=2E21, label=label, fmt='o', markersize = 7, c='C3')
            label = None
    except:
        pass
    plt.legend(loc='best')

def ChinVSTime(results, conditions):

    plt.tick_params(axis='y')
    plt.xlim(1E-3*0.8,margin_xmax*max(max(results[0]["result"].t/3600),max(results[-1]["result"].t/3600)))
    plt.ylabel(r'$\chi_n$ (nucleation)')
    plt.xlabel('Time (hours)')

    for method_id, dict_res in enumerate(results):
        X = dict_res["chi_n_times"]
        Y = dict_res["chi_n_values"]
        z = [x for _, x in sorted(zip(X, Y))] # sorted chi
        X.sort() # sorted time

        plt.ylim(-0.1, 1.1)
        plt.semilogx([x / 3600 for x in X], z, color='C'+str(method_id),label=r'$\chi_n$ '+ conditions["methods_names"][method_id] + ' model')
        # plt.semilogx([x / 3600 for x in results[2][method_id][1]], results[2][method_id][0], linestyle='dashed', color='C'+str(method_id),label=r'$\chi_g$ '+ conditions["methods"][method_id][0] +', '+ conditions["methods"][method_id][1])
    
    plt.legend(loc="best")
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # # Put a legend to the right of the current axis
    # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

def ChigVSTime(results, conditions):
    plt.tick_params(axis='y')
    plt.xlim(1E-3*0.8,margin_xmax*max(max(results[0]["result"].t/3600),max(results[-1]["result"].t/3600)))
    plt.ylabel(r'$\chi_g$ (growth)')
    plt.xlabel('Time (hours)')
    
    for method_id, dict_res in enumerate(results):
        X = dict_res["chi_g_times"]
        Y = dict_res["chi_g_values"]
        z = [x for _, x in sorted(zip(X, Y))] # sorted chi
        X.sort() # sorted time

        plt.ylim(-0.1, 1.1)
        plt.semilogx([x / 3600 for x in X], z, color='C'+str(method_id),label=r'$\chi_g$ '+ conditions["methods_names"][method_id] + ' model')

    
    plt.legend(loc="best")

def fvVSTime(results, conditions) :
    plt.xlim(1E-3*0.8,margin_xmax*max(max(results[0]["result"].t/3600),max(results[-1]["result"].t/3600)))


    for method_id, dict_res in enumerate(results):
        plt.loglog(dict_res["result"].t/3600, 100*dict_res["result"].y[4], color='C'+str(method_id), label = conditions["methods_names"][method_id] + ' model')

    plt.ylim(ymin=5E-2)
    plt.ylabel('Volume fraction of precipitates (%)')
    plt.xlabel('Time (hours)')
    plt.legend(loc='best')

def CVSTime(results, conditions):
    
    for method_id, dict_res in enumerate(results):
            plt.semilogx(dict_res["result"].t/3600, dict_res["result"].y[2]/6.021e+26, color='C'+str(method_id), label = r'$C_{Sc}^{mat}$ '+ conditions["methods_names"][method_id] + ' model')
            plt.semilogx(dict_res["result"].t/3600, dict_res["result"].y[3]/6.021e+26, color='C'+str(method_id), linestyle = 'dashed', label=r'$C_{Zr}^{mat}$ '+ conditions["methods_names"][method_id] + ' model')
    plt.xlim(1E-3*0.8,margin_xmax*max(max(results[0]["result"].t/3600),max(results[-1]["result"].t/3600)))
    plt.ylim(bottom=0)

    try :
        df = pd.read_csv(conditions["path_SANS"])
        df=df.dropna()
        compo_time_Sc = df[df.Composition == conditions["composition_name"]][['Time (h)','at pct Sc matrix APT']].values
        compo_time_Zr = df[df.Composition == conditions["composition_name"]][['Time (h)','at pct Zr matrix APT']].values
        label = "APT Sc"
        for x,y in compo_time_Sc:
            plt.errorbar(x, y, yerr=0.01, label=label, fmt='o', markersize = 7, c='C5')
            label = None
        label = "APT Zr"
        for x,y in compo_time_Zr:
            plt.errorbar(x, y, yerr=0.01, label=label, fmt='s', markersize = 7, c='C7')
            label = None
    except:
        pass

    # plt.loglog(Time,results_array[:,11],label="CSc_Interface")
    # plt.loglog(Time,results_array[:,12],label="CZr_Interface")
    plt.legend(loc='best')
    plt.ylabel('Atomic concentration in the matrix (%)')
    plt.xlabel('Time (hours)')

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)

def plotTTT(TTT, TMIN, TMAX, conditions, title):
    rcParams['font.family'] = 'serif'
    fig = plt.figure(figsize=(9,4))
    fig.suptitle(r"TTT - Al "+str(100*conditions["CSc_Matrix_initial"])+"at% Sc "\
        +str(100*conditions["CZr_Matrix_initial"])+r"at%Zr $\gamma_{Sc}$ = "\
        +str(conditions["Surface_Energy_Sc"])\
        +r"$\gamma_{Zr}$ = "+str(conditions["Surface_Energy_Zr"]))

    plt.subplot(121)
    plt.semilogx(TTT[:,2]/3600,TTT[:,0], label = 'core 20%', markersize = 2, color ='blue')
    plt.semilogx(TTT[:,3]/3600,TTT[:,0], label = 'core 80%', markersize = 2, color ='orange')
    plt.semilogx(TTT[:,4]/3600,TTT[:,0], label = 'shell 20%', markersize = 2, color ='brown')
    plt.semilogx(TTT[:,5]/3600,TTT[:,0], label = 'shell 80%', markersize = 2, color ='green')
    plt.legend(loc='best')
    plt.ylim(TMIN, TMAX)
    plt.ylabel("Temperature (°C)")
    plt.xlabel(r"Time (hours)")
    plt.grid(linestyle='dotted', alpha = 0.5)

    host = plt.subplot(122)

    par1 = host.twinx()
    par2 = host.twinx()
    par2.spines["right"].set_position(("axes", 1.2))
    make_patch_spines_invisible(par2)
    par2.spines["right"].set_visible(True)

    host.set_xlim(TMIN, TMAX)
    host.set_xlabel("Temperature (°C)")
    host.set_ylabel("Radius (m)")
    p1, = host.semilogy(TTT[:,0],TTT[:,6], label = 'R')

    p2, = par1.semilogy(TTT[:,0],TTT[:,7], label = 'N', color = "red")
    par1.tick_params(axis='y', colors=p2.get_color())
    par1.set_ylabel(r"Number ($m^{-3}$)")
    
    p3, = par2.plot(TTT[:,0],TTT[:,1]*100, label = r'$f_v max$', color = "green")
    par2.tick_params(axis='y', colors=p3.get_color())
    par2.set_ylim(0.2, 0.7)
    par2.set_ylabel(r"Volume fraction of precipitates (%)")
    # labs = [l.get_label() for l in plt_legend]
    # ax2.legend(plt_legend, labs, loc='upper right')

    lines = [p1, p2, p3]

    host.legend(lines, [l.get_label() for l in lines], loc="center left")

    plt.tight_layout()
    fig.subplots_adjust(top=0.87)
    if conditions["saveGraph"] == True:
        plt.savefig("../graphs/TTT_" +title+".pdf")
    plt.show()

def R_N_VS_Temperature(radius, number, temperature):
    
    rcParams['font.family'] = 'serif'

    plt.subplot(121)

    plt1 = plt.semilogy(temperature-273, radius,label="Radius")
    plt.ylabel('Radius of precipitates (m)')
    plt.xlabel('Temperature (°C)')
    ax2 = plt.twinx()
    plt.ylabel(r'$N (m^{-3})$')
    ax2.tick_params(axis='y')
    plt2 = ax2.semilogy(temperature-273,number, color='orange', label='Number')
    # a little hack for the legend :
    plt_legend = plt1 + plt2
    labs = [l.get_label() for l in plt_legend]
    ax2.legend(plt_legend, labs, loc='best')

    #plt.xlim(200,500)
    
    plt.subplot(122)
    plt1 = plt.plot(temperature-273, 4*np.pi / 3 * radius * radius * radius * number *100,label="fv")
    plt.ylabel('Volume fraction (%)')
    plt.xlabel('Temperature (°C)')
    plt.tight_layout()
    
    plt.show()